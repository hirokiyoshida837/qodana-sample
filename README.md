# JetBrains/Qodana GitLab Ci Sample

[![pipeline status](https://gitlab.com/hirokiyoshida837/qodana-sample/badges/master/pipeline.svg)](https://gitlab.com/hirokiyoshida837/qodana-sample/-/commits/master)

## about

This is sample project for GitLab CI with  [JetBrains/Qodana](https://github.com/JetBrains/Qodana).


## Lisence

MIT.

and, you should check the JetBrains Privay Policy

> By using Qodana, you agree to the JetBrains EAP user agreement and JetBrains privacy policy.

- [JetBrains/Qodana: Source repository of Qodana Help](https://github.com/JetBrains/Qodana#license)
